(ns challenges.easy.32-duplicate-a-sequence-test
  (:require  [clojure.test :refer :all]))

(defn dup [lst] (interleave lst lst))

(deftest tests_simple_case
  (is (= (dup [1 2]) '(1 1 2 2))))

