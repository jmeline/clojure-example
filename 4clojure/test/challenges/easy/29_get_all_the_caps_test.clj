(ns challenges.easy.29-get-all-the-caps-test
  (:require  [clojure.test :refer :all]
             [clojure.string :as str]))

;; #29
(deftest get-the-caps
  (letfn [(getAllCaps [list]
            (->> list
                 (filter #(not= (str %) (str/lower-case %)))
                 (str/join)))]
    (is (= (getAllCaps "AbCdEfG!@#hI") "ACEGI"))))
